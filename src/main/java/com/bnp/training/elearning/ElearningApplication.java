package com.bnp.training.elearning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ElearningApplication {

	private static final Logger log = LoggerFactory.getLogger(ElearningApplication.class);
	
	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(ElearningApplication.class, args);
		String[] beanDefinitionNames = configurableApplicationContext.getBeanDefinitionNames();
		log.info("****************Bean Initailized List*************************\n");
		for(String beanName : beanDefinitionNames){
			log.info(beanName+"\n");
		}
		log.info("**************************************************************\n");
	}

}

