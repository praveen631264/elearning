package com.bnp.training.elearning.administration.role;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.bnp.training.elearning.administration.user.ApplicationUser;
import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "application_role")
public class ApplicationRole extends CommonEntityFields<ApplicationRole> {

	public ApplicationRole() {
		System.out.println("*****************called main****************");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6282300620302800163L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;

	@ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<ApplicationUser> users;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationRole other = (ApplicationRole) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ApplicationUser> getUsers() {
		return users;
	}

	public void setUsers(Set<ApplicationUser> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "ApplicationRole [id=" + id + ", name=" + name + "]";
	}

}
