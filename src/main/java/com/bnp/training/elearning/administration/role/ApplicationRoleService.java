package com.bnp.training.elearning.administration.role;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationRoleService {

	@Autowired ApplicationRoleRepository applicationRoleRepository;
	
	public List<ApplicationRole> listRoles(){
		return applicationRoleRepository.findAll();
	}
	
	public ApplicationRole save(ApplicationRole role){
		return applicationRoleRepository.save(role);
	}
	
	public ApplicationRole findRoleById(Long id){
		return applicationRoleRepository.findById(id).get();
	}
}
