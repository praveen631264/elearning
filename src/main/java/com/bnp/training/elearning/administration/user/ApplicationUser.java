package com.bnp.training.elearning.administration.user;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bnp.training.elearning.administration.role.ApplicationRole;
import com.bnp.training.elearning.administration.user.activity.ApplicationUserActivity;
import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.bnp.training.elearning.valuechain.rating.ValueChainRating;
import com.bnp.training.elearning.valuechain.registration.ValueChainRegistration;

@Entity
@Table(name = "application_user")
public class ApplicationUser extends CommonEntityFields<ApplicationUser>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7365791356275977225L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	@Column(nullable = false, unique = true)
	private String username;
	private String firstName;
	private String middleName;
	private String lastName;
	private String eMail;
	private String primaryMobileNo;
	private String secondaryMobileNo;
	private String password;
	private String country;
	private String city;
	private String postalCode;
	
	@Transient
	private String confirmPassword;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<ApplicationRole> roles;

	@OneToMany
	private Set<ValueChainRegistration> valueChainRegistrations;
	
	@OneToMany
	private List<ApplicationUserActivity> applicationUserActivities;
	
	@OneToMany
	private List<ValueChainRating> valueChainRatings;
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPrimaryMobileNo() {
		return primaryMobileNo;
	}

	public void setPrimaryMobileNo(String primaryMobileNo) {
		this.primaryMobileNo = primaryMobileNo;
	}

	public String getSecondaryMobileNo() {
		return secondaryMobileNo;
	}

	public void setSecondaryMobileNo(String secondaryMobileNo) {
		this.secondaryMobileNo = secondaryMobileNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Set<ApplicationRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<ApplicationRole> roles) {
		this.roles = roles;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Set<ValueChainRegistration> getValueChainRegistrations() {
		return valueChainRegistrations;
	}

	public void setValueChainRegistrations(
			Set<ValueChainRegistration> valueChainRegistrations) {
		this.valueChainRegistrations = valueChainRegistrations;
	}

	public List<ApplicationUserActivity> getApplicationUserActivities() {
		return applicationUserActivities;
	}

	public void setApplicationUserActivities(
			List<ApplicationUserActivity> applicationUserActivities) {
		this.applicationUserActivities = applicationUserActivities;
	}

	public List<ValueChainRating> getValueChainRatings() {
		return valueChainRatings;
	}

	public void setValueChainRatings(List<ValueChainRating> valueChainRatings) {
		this.valueChainRatings = valueChainRatings;
	}


	
}
