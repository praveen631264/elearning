package com.bnp.training.elearning.administration.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ApplicationUserService {

	@Autowired ApplicationUserRepository applicationUserRepository;
	@Autowired PasswordEncoder encoder;
	
	public ApplicationUser findByUsername(String username){
		return applicationUserRepository.findByUsername(username);
	}
	
	public List<ApplicationUser> listUsers(){
		return applicationUserRepository.findAll();
	}
	
	public ApplicationUser save(ApplicationUser user){
		user.setPassword(encoder.encode(user.getPassword()));
		return applicationUserRepository.save(user);
	}
	
}
