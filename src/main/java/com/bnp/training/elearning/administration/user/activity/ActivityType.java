package com.bnp.training.elearning.administration.user.activity;

public enum ActivityType {
	VALUECHAIN_BOOKMARK, VALUE_CHAIN_HISTORY, LOGIN, LOGOUT
}
