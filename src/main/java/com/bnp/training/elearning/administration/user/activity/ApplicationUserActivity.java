package com.bnp.training.elearning.administration.user.activity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.bnp.training.elearning.valuechain.ValueChain;

@Entity
@Table(name = "application_user_activity")
public class ApplicationUserActivity extends CommonEntityFields<ApplicationUserActivity>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4942896823073272274L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	private ValueChain valueChain;

	@Enumerated(EnumType.STRING)
	private ActivityType activityType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ValueChain getValueChain() {
		return valueChain;
	}

	public void setValueChain(ValueChain valueChain) {
		this.valueChain = valueChain;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	@Override
	public String toString() {
		return "ApplicationUserActivity [id=" + id + ", valueChain="
				+ valueChain + ", activityType=" + activityType + ", getId()="
				+ getId() + ", getValueChain()=" + getValueChain()
				+ ", getActivityType()=" + getActivityType()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()="
				+ getCreatedDate() + ", getLastModifiedBy()="
				+ getLastModifiedBy() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", isDeleteFlag()=" + isDeleteFlag()
				+ ", getState()=" + getState() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
