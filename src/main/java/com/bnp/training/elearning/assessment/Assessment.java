package com.bnp.training.elearning.assessment;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;

@Entity
@Table(name = "assessment")
public class Assessment extends CommonEntityFields<Assessment>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1656467505468037224L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String description;
	
	@OneToMany
	private List<Question> questions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Assessment [id=" + id + ", description=" + description
				+ ", questions=" + questions + ", getId()=" + getId()
				+ ", getDescription()=" + getDescription()
				+ ", getQuestions()=" + getQuestions() + ", getCreatedBy()="
				+ getCreatedBy() + ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", isDeleteFlag()=" + isDeleteFlag() + ", getState()="
				+ getState() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

}
