package com.bnp.training.elearning.assessment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;

@Entity
@Table(name = "question_option")
public class Option extends CommonEntityFields<Option>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8297534043325475793L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String optionText;
	private String description;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOptionText() {
		return optionText;
	}
	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "Option [id=" + id + ", optionText=" + optionText
				+ ", description=" + description + ", getId()=" + getId()
				+ ", getOptionText()=" + getOptionText()
				+ ", getDescription()=" + getDescription()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()="
				+ getCreatedDate() + ", getLastModifiedBy()="
				+ getLastModifiedBy() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", isDeleteFlag()=" + isDeleteFlag()
				+ ", getState()=" + getState() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
}
