package com.bnp.training.elearning.assessment;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;

@Entity
@Table(name = "question")
public class Question extends CommonEntityFields<Question>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3983702302555567629L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany
	private List<Option> options;
	
	private boolean isMultiChoice;
	
	@OneToMany
	private List<Option> answers;
	
	private String hint;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public boolean isMultiChoice() {
		return isMultiChoice;
	}

	public void setMultiChoice(boolean isMultiChoice) {
		this.isMultiChoice = isMultiChoice;
	}

	public List<Option> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Option> answers) {
		this.answers = answers;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", options=" + options
				+ ", isMultiChoice=" + isMultiChoice + ", answers=" + answers
				+ ", hint=" + hint + ", getId()=" + getId() + ", getOptions()="
				+ getOptions() + ", isMultiChoice()=" + isMultiChoice()
				+ ", getAnswers()=" + getAnswers() + ", getHint()=" + getHint()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()="
				+ getCreatedDate() + ", getLastModifiedBy()="
				+ getLastModifiedBy() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", isDeleteFlag()=" + isDeleteFlag()
				+ ", getState()=" + getState() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

	
	
}
