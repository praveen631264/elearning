package com.bnp.training.elearning.common.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Audit {


	private static final Logger log = LoggerFactory.getLogger(Audit.class);
	private static Audit instance = null;
	
	private Audit(){}
	
	static{
		try{
			instance = new Audit();
		}catch(Exception e){
			throw new RuntimeException("Exception occured in creating singleton instance", e);
		}
	}
	
	public static Audit getInstance(){
		return instance;
	}
	
	public <T> void findDifferenceAndlog(T oldObj, T newObj, AuditActionType type){
		log.info("oldObj ====> "+oldObj);
		log.info("newObj ====> "+newObj);
		log.info("action type ===> " + type.name());
		
	}
	
	public void log(String tableName, String acion, String oldValue, String newValue){
		
	}
}
