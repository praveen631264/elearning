package com.bnp.training.elearning.common.audit;

public enum AuditActionType {
	INSERTED, UPDATED, DELETED
}
