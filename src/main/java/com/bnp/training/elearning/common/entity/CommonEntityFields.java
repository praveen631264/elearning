package com.bnp.training.elearning.common.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.bnp.training.elearning.common.audit.Audit;
import com.bnp.training.elearning.common.audit.AuditActionType;

@MappedSuperclass
public abstract class CommonEntityFields<T>  implements Serializable{

	
	public CommonEntityFields() {
	System.out.println("*****************called****************");
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CommonEntityFields.class);
	
	
	@CreatedBy
	private String createdBy;
	
	@CreatedDate
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@LastModifiedBy
	private String lastModifiedBy;
	
	@LastModifiedDate
	@Temporal(TemporalType.DATE)
	private Date lastModifiedDate;
	
	private boolean deleteFlag;
	
	@Enumerated(EnumType.STRING)
	private State state;
	
	@Transient
	private T oldObj;
    
    @SuppressWarnings("unchecked")
	@PostLoad
    private void saveState(){
    	this.oldObj = (T) SerializationUtils.clone(this);
    }
    
    @PostUpdate
	public void postUpdate(){
    	Audit.getInstance().findDifferenceAndlog(oldObj, this, AuditActionType.UPDATED);
    }

    @PostPersist
	public void postPersist(){
		Audit.getInstance().findDifferenceAndlog(null, this, AuditActionType.INSERTED);
	}
    
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CommonEntityFields [createdBy=" + createdBy + ", createdDate="
				+ createdDate + ", lastModifiedBy=" + lastModifiedBy
				+ ", lastModifiedDate=" + lastModifiedDate + ", deleteFlag="
				+ deleteFlag + ", state=" + state + "]";
	}
	
}
