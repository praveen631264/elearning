package com.bnp.training.elearning.common.entity;

public enum State {
	ACTIVE, INACTIVE
}
