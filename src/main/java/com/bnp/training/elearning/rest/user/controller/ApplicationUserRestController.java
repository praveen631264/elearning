package com.bnp.training.elearning.rest.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bnp.training.elearning.administration.user.ApplicationUser;
import com.bnp.training.elearning.administration.user.ApplicationUserService;

@RestController
@RequestMapping("/user")
public class ApplicationUserRestController {

	@Autowired ApplicationUserService applicationUserService; 
	
	@GetMapping("/loggedInUserInfo")
	public ApplicationUser getLoggedInUserInfo(Authentication authentication){
		return applicationUserService.findByUsername(authentication.getName());
	}
	
	@GetMapping
	public List<ApplicationUser> listUsers(){
		return applicationUserService.listUsers();
	}
	
	@PutMapping
	public ApplicationUser save(@RequestBody ApplicationUser applicationUser){
		return applicationUserService.save(applicationUser);
	}
	

}
