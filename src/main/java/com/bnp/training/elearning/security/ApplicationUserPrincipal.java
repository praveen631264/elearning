package com.bnp.training.elearning.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.bnp.training.elearning.administration.user.ApplicationUser;

public class ApplicationUserPrincipal implements UserDetails {


	private static final Logger log = LoggerFactory.getLogger(ApplicationUserPrincipal.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6275903426638259916L;
	private final ApplicationUser user;

	//

	public ApplicationUserPrincipal(ApplicationUser user) {
		this.user = user;
	}

	//

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		user.getRoles().forEach(obj -> authorities.add(new SimpleGrantedAuthority(obj.getName())));
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	//

	public ApplicationUser getApplicationUser() {
		return user;
	}

}