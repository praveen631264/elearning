package com.bnp.training.elearning.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;

import com.bnp.training.elearning.administration.user.ApplicationUser;
import com.bnp.training.elearning.administration.user.ApplicationUserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private ApplicationUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		ApplicationUser user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new ApplicationUserPrincipal(user);
	}
}