package com.bnp.training.elearning.valuechain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;

@Entity
@Table(name = "scorecard")
public class ScoreCard extends CommonEntityFields<ScoreCard>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2471179696780799870L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private Integer rank;
	private Integer weight;
	
	@Enumerated(EnumType.STRING)
	private ScoreResult result;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public ScoreResult getResult() {
		return result;
	}
	public void setResult(ScoreResult result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "ScoreCard [id=" + id + ", name=" + name + ", rank=" + rank
				+ ", weight=" + weight + ", result=" + result + ", getId()="
				+ getId() + ", getName()=" + getName() + ", getRank()="
				+ getRank() + ", getWeight()=" + getWeight() + ", getResult()="
				+ getResult() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", isDeleteFlag()=" + isDeleteFlag() + ", getState()="
				+ getState() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

}
