package com.bnp.training.elearning.valuechain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bnp.training.elearning.administration.user.ApplicationUser;
import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.bnp.training.elearning.valuechain.chapter.Chapter;

@Entity
@Table(name = "value_chain")
public class ValueChain extends CommonEntityFields<ValueChain>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1696383853819612107L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String title;
	private String description;
	private String[] tags;
	private Long parentId;
	private boolean isPreviousChapterMandatory;
	
	@Enumerated(EnumType.STRING)
	private ValueChainState valueChainState;
	
	@OneToOne
	private ApplicationUser author;
	
	private Integer orderIndex;
	
	@OneToMany
	private List<Chapter> chapters;
	
	@OneToMany
	private List<ScoreCard> scoreCards;
	
	@Temporal(TemporalType.DATE)
	private Date expiry;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public boolean isPreviousChapterMandatory() {
		return isPreviousChapterMandatory;
	}

	public void setPreviousChapterMandatory(boolean isPreviousChapterMandatory) {
		this.isPreviousChapterMandatory = isPreviousChapterMandatory;
	}

	public ApplicationUser getAuthor() {
		return author;
	}

	public void setAuthor(ApplicationUser author) {
		this.author = author;
	}

	public Integer getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	public List<ScoreCard> getScoreCards() {
		return scoreCards;
	}

	public void setScoreCards(List<ScoreCard> scoreCards) {
		this.scoreCards = scoreCards;
	}

	public Date getExpiry() {
		return expiry;
	}

	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	public ValueChainState getValueChainState() {
		return valueChainState;
	}

	public void setValueChainState(ValueChainState valueChainState) {
		this.valueChainState = valueChainState;
	}

	@Override
	public String toString() {
		return "ValueChain [id=" + id + ", name=" + name + ", title=" + title
				+ ", description=" + description + ", tags="
				+ Arrays.toString(tags) + ", parentId=" + parentId
				+ ", isPreviousChapterMandatory=" + isPreviousChapterMandatory
				+ ", valueChainState=" + valueChainState + ", author=" + author
				+ ", orderIndex=" + orderIndex + ", chapters=" + chapters
				+ ", scoreCards=" + scoreCards + ", expiry=" + expiry
				+ ", getId()=" + getId() + ", getName()=" + getName()
				+ ", getTitle()=" + getTitle() + ", getDescription()="
				+ getDescription() + ", getTags()="
				+ Arrays.toString(getTags()) + ", getParentId()="
				+ getParentId() + ", isPreviousChapterMandatory()="
				+ isPreviousChapterMandatory() + ", getAuthor()=" + getAuthor()
				+ ", getOrderIndex()=" + getOrderIndex() + ", getChapters()="
				+ getChapters() + ", getScoreCards()=" + getScoreCards()
				+ ", getExpiry()=" + getExpiry() + ", getValueChainState()="
				+ getValueChainState() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", isDeleteFlag()=" + isDeleteFlag() + ", getState()="
				+ getState() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

}
