package com.bnp.training.elearning.valuechain;

public enum ValueChainState {

	REVIEW, APPROVED, REJECTED
	
}
