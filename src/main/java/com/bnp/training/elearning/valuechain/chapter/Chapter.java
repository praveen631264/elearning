package com.bnp.training.elearning.valuechain.chapter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;

@Entity
@Table(name = "value_chain_chapter")
public class Chapter extends CommonEntityFields<Chapter>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1521471832369786397L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String title;
	private String description;
	
	@Enumerated(EnumType.STRING)
	private ChapterFileFormat fileFomat;
	
	private Integer chapterDuration;
	private Integer NoOfPages;
	private String filePath;
	private Integer orderIndex;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ChapterFileFormat getFileFomat() {
		return fileFomat;
	}
	public void setFileFomat(ChapterFileFormat fileFomat) {
		this.fileFomat = fileFomat;
	}
	public Integer getChapterDuration() {
		return chapterDuration;
	}
	public void setChapterDuration(Integer chapterDuration) {
		this.chapterDuration = chapterDuration;
	}
	public Integer getNoOfPages() {
		return NoOfPages;
	}
	public void setNoOfPages(Integer noOfPages) {
		NoOfPages = noOfPages;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Integer getOrderIndex() {
		return orderIndex;
	}
	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}
	@Override
	public String toString() {
		return "Chapter [id=" + id + ", name=" + name + ", title=" + title
				+ ", description=" + description + ", fileFomat=" + fileFomat
				+ ", chapterDuration=" + chapterDuration + ", NoOfPages="
				+ NoOfPages + ", filePath=" + filePath + ", orderIndex="
				+ orderIndex + ", getId()=" + getId() + ", getName()="
				+ getName() + ", getTitle()=" + getTitle()
				+ ", getDescription()=" + getDescription()
				+ ", getFileFomat()=" + getFileFomat()
				+ ", getChapterDuration()=" + getChapterDuration()
				+ ", getNoOfPages()=" + getNoOfPages() + ", getFilePath()="
				+ getFilePath() + ", getOrderIndex()=" + getOrderIndex()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()="
				+ getCreatedDate() + ", getLastModifiedBy()="
				+ getLastModifiedBy() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", isDeleteFlag()=" + isDeleteFlag()
				+ ", getState()=" + getState() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
}
