package com.bnp.training.elearning.valuechain.chapter;

public enum ChapterFileFormat {
	THIRDPARTY_URL, BNP_COURSE, PDF, WORD, TEXT, PPT, INTERNAL
}
