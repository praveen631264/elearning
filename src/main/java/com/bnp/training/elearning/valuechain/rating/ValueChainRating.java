package com.bnp.training.elearning.valuechain.rating;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bnp.training.elearning.administration.user.ApplicationUser;
import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.bnp.training.elearning.valuechain.ValueChain;

@Entity
@Table(name = "value_chain_rating")
public class ValueChainRating extends CommonEntityFields<ValueChainRating>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8989665603712214502L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private ApplicationUser user;
	
	@OneToOne
	private ValueChain valueChain;
	
	private Integer rating;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ApplicationUser getUser() {
		return user;
	}
	public void setUser(ApplicationUser user) {
		this.user = user;
	}
	public ValueChain getValueChain() {
		return valueChain;
	}
	public void setValueChain(ValueChain valueChain) {
		this.valueChain = valueChain;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "ValueChainRating [id=" + id + ", user=" + user
				+ ", valueChain=" + valueChain + ", rating=" + rating
				+ ", getId()=" + getId() + ", getUser()=" + getUser()
				+ ", getValueChain()=" + getValueChain() + ", getRating()="
				+ getRating() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", isDeleteFlag()=" + isDeleteFlag() + ", getState()="
				+ getState() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
}
