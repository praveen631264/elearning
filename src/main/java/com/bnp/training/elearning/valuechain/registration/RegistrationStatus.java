package com.bnp.training.elearning.valuechain.registration;

public enum RegistrationStatus {
	REGISTERED, INPROGRESS, COMPLETED, INCOMPLETE
}
