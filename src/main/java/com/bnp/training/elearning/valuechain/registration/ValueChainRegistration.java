package com.bnp.training.elearning.valuechain.registration;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bnp.training.elearning.common.entity.CommonEntityFields;
import com.bnp.training.elearning.valuechain.ScoreCard;
import com.bnp.training.elearning.valuechain.ValueChain;

@Entity
@Table(name = "value_chain_registration")
public class ValueChainRegistration extends CommonEntityFields<ValueChainRegistration>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5859347955172880584L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private ValueChain valueChain;
	
	@Enumerated(EnumType.STRING)
	private RegistrationStatus registrationStatus;
	
	@OneToOne
	private ScoreCard score;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueChainRegistration other = (ValueChainRegistration) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ValueChain getValueChain() {
		return valueChain;
	}

	public void setValueChain(ValueChain valueChain) {
		this.valueChain = valueChain;
	}

	public RegistrationStatus getRegistrationStatus() {
		return registrationStatus;
	}

	public void setRegistrationStatus(RegistrationStatus registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	public ScoreCard getScore() {
		return score;
	}

	public void setScore(ScoreCard score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "ValueChainRegistration [id=" + id + ", valueChain="
				+ valueChain + ", registrationStatus=" + registrationStatus
				+ ", score=" + score + ", hashCode()=" + hashCode()
				+ ", getId()=" + getId() + ", getValueChain()="
				+ getValueChain() + ", getRegistrationStatus()="
				+ getRegistrationStatus() + ", getScore()=" + getScore()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()="
				+ getCreatedDate() + ", getLastModifiedBy()="
				+ getLastModifiedBy() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", isDeleteFlag()=" + isDeleteFlag()
				+ ", getState()=" + getState() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}

}
