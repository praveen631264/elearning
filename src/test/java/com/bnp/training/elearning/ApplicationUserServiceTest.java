package com.bnp.training.elearning;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bnp.training.elearning.administration.role.ApplicationRole;
import com.bnp.training.elearning.administration.role.ApplicationRoleService;
import com.bnp.training.elearning.administration.user.ApplicationUserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationUserServiceTest {

	@Autowired ApplicationUserService userService;
	@Autowired ApplicationRoleService roleService;
	
	@Test
	public void save(){
		ApplicationRole role = new ApplicationRole();
		role.setId(10L);
		role.setName("ADMIN");
		roleService.save(role);
		/*ApplicationRole role = roleService.findRoleById(3L);
		ApplicationUser user = new ApplicationUser();
		user.setUsername("c45550");
		user.setFirstName("Praveen");
		user.setMiddleName("Kumar");
		user.setLastName("Prabhakaran");
		user.setPassword("praveen");
		user.setPrimaryMobileNo("8754260221");
		user.seteMail("praveen631264@gmail.com");
		Set<ApplicationRole> roles = new HashSet<ApplicationRole>();
		roles.add(role);
		user.setRoles(roles);
		userService.save(user);*/
	}
	
}
